package fr.intech.tondeuse;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.assertj.core.api.Assertions.assertThat;

public class MainTest {
    @Test
    void testProgramme() throws URISyntaxException, IOException {
        var result = Main.run(getClass().getClassLoader().getResource("programme.txt"));

        assertThat(result).isEqualTo("1 3 N\n5 1 E\n");
    }
}
