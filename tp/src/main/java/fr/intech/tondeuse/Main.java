package fr.intech.tondeuse;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, URISyntaxException {
        System.out.println(run(new URL("file:" + args[0])));
    }

    public static String run(final URL input) throws URISyntaxException, IOException {
        var lines = Files.readAllLines(Path.of(input.toURI()));

        var scanner = new Scanner(lines.get(0));
        var maxX = scanner.nextInt();
        var maxY = scanner.nextInt();


        var result = new StringBuilder();
        for (int i = 1; i < lines.size(); i += 2) {
            scanner = new Scanner(lines.get(i));
            var x = scanner.nextInt();
            var y = scanner.nextInt();
            var o = scanner.next().trim();

            var is = lines.get(i + 1).split("");
            for (final String ii : is) {
                switch (ii) {
                    case "A":
                        switch (o) {
                            case "N":
                                if (y < maxY) {
                                    y++;
                                }
                                break;
                            case "E":
                                if (x < maxX) {
                                    x++;
                                }
                                break;
                            case "S":
                                if (y > 0) {
                                    y--;
                                }
                                break;
                            case "W":
                                if (x > 0) {
                                    x--;
                                }
                                break;
                        }
                        break;
                    case "D":
                        switch (o) {
                            case "N" -> o = "E";
                            case "E" -> o = "S";
                            case "S" -> o = "W";
                            case "W" -> o = "N";
                        }
                        break;
                    case "G":
                        switch (o) {
                            case "N" -> o = "W";
                            case "W" -> o = "S";
                            case "S" -> o = "E";
                            case "E" -> o = "N";
                        }
                        break;
                }
            }
            result.append(String.format("%d %d %s\n", x, y, o));
        }
        return result.toString();
    }
}
