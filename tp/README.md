# Solid Application Design : Travaux Pratiques

Au travers de ce TP, vous allez améliorer un petit programme qui simule le fonctionnement de tondeuses à gazons
automatisées déployées sur une pelouse. Cet exercice vous permettra de mettre en œuvre les concepts et principes vus en
cours.

Vous serez beaucoup guidés pour les premiers exercices, puis de moins en moins. Cela étant dit, le même niveau
d'application et de rigueur sont attendus pour les derniers exercices que pour les premiers.

Si vous le souhaitez, vous pouvez
garder [le support de cours](https://esiha-intech.gitlab.io/s04-solid-application-design/cours) ouvert à côté de vous.
La navigation se fait avec les flèches du clavier :

* horizontalement pour naviguer entre les chapitres ;
* verticalement pour naviguer au sein d'un chapitre.

## Préparation

Vous aurez besoin d'un [JDK 17](https://jdk.java.net/17/). Si vous préférez un installateur tout-en-un, vous pouvez
utiliser [celui non-libre d'Oracle](https://www.oracle.com/java/technologies/downloads).

Vous aurez aussi besoin d'un compte Gitlab pour forker ce projet. Vous n'aurez plus qu'à me fournir le nom de lien vers
votre fork pour me soumettre vos exercices.

Validez que tout est en ordre en lançant, depuis le dossier `tp/`, la commande `./mvnw test` (`.\mvnw.cmd test` sous
Windows).

Enfin, utilisez l'IDE de votre choix pour travailler.

## Contexte

### Présentation

La société MowItNow est une petite start-up qui propose un système de tondeuses automatisées programmables. Le CEO a
fait appel son petit neveu pour l'implémentation de la première version du système de contrôle des tondeuses. Avec le
succès grandissant du produit de MowItNow, le CEO a décidé d'investir pour le développement de nouvelles
fonctionnalités. Avant de vous en confier la réalisation, le CEO vous a assuré que son petit neveu avait tout bien testé
le fonctionnement actuel.

Actuellement, le système MowItNow permet de piloter des tondeuses sur une pelouse rectangulaire. La surface de pelouse
est repérée par un quadrillage imaginaire orthonormé entier. Le rectangle à tondre est considéré comme ayant son coin
inférieur gauche au point de coordonnées (0, 0) et son coin supérieur droit (X, Y) est configurable.

Des tondeuses sont alors placées sur la pelouse à tondre et le système MowItNow est informé, pour chaque tondeuse :

* de sa position de départ, sous la forme d'un point (X, Y) sur la pelouse à tondre et de son orientation selon l'un des
  4 points cardinaux (Nord, Sud, Est, Ouest) ;
* d'une liste d'instructions à exécuter dans l'ordre. Ces instructions peuvent être :
    * avancer d'une case dans le sens de l'orientation de la tondeuse ;
    * tourner d'un quart de tour vers la gauche ;
    * tourner d'un quart de tour vers la droite.

Le programme des tondeuses est exécuté dans l'ordre demandé avec une vérification simple : si une instruction ferait
sortir la tondeuse de la pelouse à tondre, l'instruction est ignorée et la tondeuse passe à l'instruction suivante.

En fin d'exécution, la position finale des tondeuses est obtenue, dans l'ordre.

### Configuration sous forme de fichier

La configuration complète soumise au système MowItNow sous la forme d'un fichier texte construit de la façon suivante :

```
X Y
x1 y1 o1
i1i1i1i1i1i1...
x2 y2 o2
i2i2i2i2i2i2...
...
```

X et Y sont les coordonnées du coin supérieur droit du rectangle à tondre.

x1 y1 o1 représente la position de départ de la première tondeuse. Les valeurs acceptées pour l'orientation de la
tondeuse sont `N` pour Nord, `E` pour Est, `S` pour Sud et `W` pour Ouest.

i1... est la liste d'instructions de la première tondeuse. Les valeurs acceptées sont `A` pour avancer une fois, `G`
pour tourner à gauche et `D` pour tourner à droite.

Les deux lignes suivantes décrivent la deuxième tondeuse et ainsi de suite.

Vous trouverez un exemple de programme compris par le système MowItNow
dans [les ressources de test](src/test/resources/programme.txt).

### Fonctionnalités à ajouter

Le CEO souhaite que vous implémentiez plusieurs nouvelles fonctionnalités. (Ça n'est pas l'énoncé des exercices,
simplement le but final à atteindre).

#### Gestion des collisions

Actuellement les tondeuses évoluent sans se soucier de la présence d'autres tondeuses. Il faudra désormais n'exécuter
une instruction que si elle ne provoque pas de collision avec une autre tondeuse, toujours en restant dans les limites
de la pelouse à tondre.

#### Gestion des erreurs

Actuellement, le système MowItNow ne fait que très peu de vérifications sur la validité du programme qui lui est soumis
pour exécution. De nombreux clients remontent que ça provoque des comportements assez bizarres quand le programme est
invalide. Il faudra faire en sorte de n'exécuter un programme que si sa description est valide :

* la position de départ des tondeuses doit être sur la pelouse à tondre
* toute valeur invalide en entrée (instruction, position, orientation, etc.) devra être rejetée avant d'exécuter quoi
  que ce soit.

#### Gestion d'autres formes de pelouses

Beaucoup de pelouses ont des formes qui ne sont pas rectangulaires. Le CEO voudrait que le système MowItNow puisse gérer
des pelouses de différentes formes.

#### Commande à distance

Dans un monde d'objets connectés, le CEO voudrait que le système MowItNow puisse être piloté via une API REST en plus
des possibilités existantes par fichier.

### Exercices

#### Comprendre le code actuel

Prenez le temps de bien comprendre comment fonctionne le code du système MowItNow, décrit
dans [la classe Main](src/main/java/fr/intech/tondeuse/Main.java).

Que pensez-vous qu'il se passerait si vous tentiez d'exécuter un programme qui contient un nombre pair de lignes ?

Que pensez-vous qu'il se passerait si la position initiale d'une tondeuse était située en dehors de la pelouse ?

Que pensez-vous qu'il se passerait si une instruction inconnue était fournie à une tondeuse ?

Que pensez-vous qu'il se passerait si une orientation invalide était fournie à une tondeuse ?

Que pensez-vous qu'il se passerait si on lançait le système MowItNow sans lui fournir de chemin vers un programme à
charger ?

Confrontez à la réalité vos réponses aux questions précédentes en écrivant des tests supplémentaires dans
̀MainTest.java`.

Vous pouvez ensuite supprimer ces tests.

#### Prendre la mesure de la maintenabilité

Pour chacune des nouvelles fonctionnalités attendues, réfléchissez à comment vous feriez pour les mettre en œuvre dans
le code actuel, sans le refactoriser.

Lesquelles vous paraissent simples à implémenter ?

Lesquelles vous paraissent compliquées à implémenter ? Pourquoi ?

D'après IntelliJ IDEA coverage runner, les tests couvrent 97% des lignes de code (36 lignes sur 37). Pensez-vous que
cela signifie que le code est bien testé, maintenable ?

Le code actuel viole les principes suivants :

* Forte cohésion/faible couplage ;
* Single Responsibility Principle (SRP) ;
* Open/Close Principle (OCP) ;
* Dependency Inversion Principle (DIP).

D'après vous, en quoi les viole-t-il ?

#### Concepts métier manipulés

La partie [Contexte](#contexte) utilise les termes suivants pour décrire les concepts et principes de fonctionnement du
système MowItNow :

* Point
* Position
* Orientation
* Pelouse
* Tondeuse
* Instruction
* Programme

Pour rendre le code maintenable, vous allez implémenter ces concepts et les comportements qui y sont associés.

Pour ce qui suit, il vous est demandé d'appliquer la méthode TDD pour écrire du code qui soit convenablement testé.

##### Point

Créez un dossier `src/main/java/fr/intech/tondeuse/domain` et un dossier `src/test/java/fr/intech/tondeuse/domain`.

Implémentez un record public `Point(int x, int y)` offrant l'opération suivante :

* `public Point plus(Point other)` calcule l'addition de 2 Points comme étant un Point dont les x et y sont les sommes
  respectives des x et y des paramètres.

Utilisez Point pour refactoriser le code historique.

##### Heading

Implémentez une enum publique `Heading` dont les seules valeurs admises sont : `NORTH`
, `EAST`, `SOUTH` et `WEST` et offrant les opérations suivantes :

* `public Heading rotate90DegreesLeft()` renvoyant le point cardinal situé à gauche en faisant une rotation de 90
  degrés.
* `public Heading rotate90DegreesRight()` renvoyant le point cardinal situé à droite en faisant une rotation de 90
  degrés.

Utilisez Heading pour refactoriser le code historique.

##### Position

Implémentez un record public `Position(Point point, Heading heading)` offrant les opérations suivantes (vous pouvez
ajouter un constructeur secondaire `Position(int x, int y, Heading heading)` pour rendre l'écriture de tests plus
agréable) :

* `public Position rotate90DegreesLeft()` renvoyant une Position de même Point, mais dont l'orientation est tournée de
  90 degrés vers la gauche.
* `public Position rotate90DegreesRight()` renvoyant une Position de même Point, mais dont l'orientation est tournée de
  90 degrés vers la droite.
* `public Position translateOnce()` renvoyant une Position de même orientation, mais dont le Point est :
    * Point(x, y + 1) si l'orientation est NORTH ;
    * Point(x + 1, y) si l'orientation est EAST ;
    * Point(x, y - 1) si l'orientation est SOUTH ;
    * Point(x - 1, y) si l'orientation est WEST.

Vous avez peut-être remarqué que l'Open/Close Principle est violé par le fait d'intégrer un switch sur l'enum `Heading`
dans le code de `translateOnce`. Réglez ce problème en implémentant une méthode `Point asDirectionPoint()` sur
l'enum `Heading` qui renvoie :

* Point(0, 1) pour NORTH ;
* Point(1, 0) pour EAST ;
* Point(0, -1) pour SOUTH ;
* Point(-1, 0) pour WEST.

Utilisez cette méthode `asDirectionPoint` pour refactoriser la méthode `translateOnce`.

Utilisez Position pour refactoriser le code historique.

##### Lawn

Ajoutez à Point les méthodes suivantes :

* `public boolean isInUpperRightQuadrantOf(Point other)` qui renvoie `true` si et seulement
  si `this.x >= other.x && this.y >= other.y`. Prenez soin d'avoir des tests pour tous les cas possibles.
* `public boolean isInLowerLeftQuadrantOf(Point other)` qui renvoie `true` si et seulement
  si `this.x <= other.x && this.y <= other.y`. Prenez soin d'avoir des tests pour tous les cas possibles.

Implémentez un record public `Lawn(Point topRightCorner)` offrant une méthode :

* `public boolean contains(Point point)` qui renvoie `true` si et seulement si le Point fourni en paramètre est dans le
  quadrant en haut à droite de l'origine (`Point(0, 0)`) et dans le quadrant en bas à gauche du `topRightCorner`.

Utilisez Lawn pour refactoriser le code historique.

##### Mower

Créez une enum publique `Instruction` dont les seules valeurs admises sont `MOVE_ONCE`, `TURN_LEFT` et `TURN_RIGHT`.

Créez une classe publique finale `Mower`, construite à partir d'une Position initiale et d'une liste d'Instructions et
offrant les méthodes :

* `public void run(Lawn lawn)` qui exécute le programme de la tondeuse sans sortir des limites de la pelouse.
* `public Position getCurrentPosition()` qui renvoie la position actuelle de la tondeuse.

Refactorisez Mower pour régler la violation de l'OCP sur Instruction.

Utilisez Mower pour refactoriser le code historique.

##### Pilot

Créez une classe publique finale `Pilot`, construite à partir d'une `Lawn` et d'une liste de `Mower`s et qui offre les
méthodes :

* `public void run()` qui exécute le programme des tondeuses dans l'ordre.
* `public List<Position> getMowersCurrentPosition()` qui renvoie la liste des positions actuelles des tondeuses.

Utilisez Pilot pour refactoriser le code historique.

#### Ré-évaluation de la maintenabilité

Reprenez la liste des nouvelles fonctionnalités voulues par le CEO. Pour chacune d'elles, réfléchissez à comment vous
vous y prendriez pour les implémenter.

Quels bénéfices voyez-vous au travail de refactorisation que vous avez mené durant les précédents exercices ?

S'il vous reste du temps, lancez-vous dans l'implémentation des fonctionnalités (du plus simple au plus compliqué) :

* Gestion des collisions ;
* Gestion des erreurs ;
* Gestion d'autres formes de pelouses (vous pouvez envisager de préfixer la première ligne du fichier par un mot-clef ou
  une lettre désignant la forme de pelouse) : cercle, carré, ...
* Commande à distance avec une API REST.
