+++

title = "S04 - Solid Application Design"
outputs = ["Reveal"]

+++

# Solid Application Design

* [Cours](cours)
* [Travaux pratiques](https://gitlab.com/esiha-intech/s04-solid-application-design/-/tree/main/tp)
