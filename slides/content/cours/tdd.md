+++

weight = 5

+++

{{% section %}}

## Test-Driven Development (TDD)

Des tests utiles, sans avoir la flemme.

---

### Définition

Le TDD est une approche itérative du développement qui promeut l'intégration au plus tôt (_shift left_) des
considérations de fonctionnalité, maintenabilité et qualité du code.

---

### Un peu de vocabulaire

* **Code de production** \
  Ce qui ira _in fine_ en production.
* **Code de test** \
  Ce qui sert à tester le code de production.
* **Refactorisation** \
  Modifier du code sans en modifier le contrat.

---

### Les 3 étapes du TDD

1. Rouge
2. Vert
3. Refactorisation

---

#### Rouge

Écrire un test qui décrit un nouveau comportement voulu.

{{% fragment %}}Vérifier qu'il échoue (il est rouge).{{% /fragment %}}

---

Rouge : nouveau comportement voulu, inexistant

```java
import org.junit.jupiter.api.Test;

import java.util.Currency;

import static org.assertj.core.api.Assertions.assertThat;

class MoneyTest {
    @Test
    void shouldAddValuesTogether() {
        assertThat(
                new Money(2, Currency.getInstance("EUR")).plus(new Money(3, Currency.getInstance("EUR")))
        ).isEqualTo(
                new Money(5, Currency.getInstance("EUR"))
        );
    }
}
```

---

#### Vert

Écrire le code de production correspondant le plus simplement.

{{% fragment %}}Vérifier que le test écrit à l'étape Rouge passe désormais (il est vert).{{% /fragment %}}

---

Vert : implémentation la plus simple

```java
import java.util.Currency;
import java.util.Objects;

record Money(int value, Currency currency) {
    Money {
        requirePositiveOrZero(value);
        Objects.requireNonNull(currency);
    }

    private static void requirePositiveOrZero(int value) {
        if (value < 0) {
            throw new IllegalArgumentException("value is negative: " + value);
        }
    }

    Money plus(Money other) {
        return new Money(this.value + other.value, this.currency);
    }
}
```

---

#### Refactorisation

Refactoriser le code de production pour améliorer la qualité.

{{% fragment %}}Les tests doivent rester verts.{{% /fragment %}}

{{% fragment %}}Le code de test peut aussi être refactorisé, avec une extrême prudence.{{% /fragment %}}

---

Refactor : la lisibilité prime !

```java
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Currency;

import static org.assertj.core.api.Assertions.assertThat;

class MoneyTest {
    @Nested
    class AddTest {
        @Test
        void shouldAddValuesTogether() {
            assertThat(euros(2).plus(euros(3))).isEqualTo(euros(5));
        }
    }

    private static Money euros(int value) {
        return new Money(value, Currency.getInstance("EUR"));
    }
}
```

Il est primordial de maîtriser les possibilités de refactorisation offertes par votre IDE !

---

Et c'est reparti pour un tour...

---

Rouge : décrire un nouveau comportement voulu

```java
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Currency;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

class MoneyTest {
    @Nested
    class AddTest {
        @Test
        void shouldAddValuesTogether() {
            assertThat(euros(2).plus(euros(3))).isEqualTo(euros(5));
        }

        @Test
        void shouldFailToAddWhenCurrenciesDiffer() {
            assertThatIllegalArgumentException().isThrownBy(() ->
                    euros(3).plus(new Money(2, Currency.getInstance("USD")))
            );
        }
    }

    private static Money euros(final int value) {
        return new Money(value, Currency.getInstance("EUR"));
    }
}
```

---

Vert : implémentation la plus simple

```java
import java.util.Currency;
import java.util.Objects;

record Money(int value, Currency currency) {
    Money {
        requireNonNegative(value);
        Objects.requireNonNull(currency, "currency is null");
    }

    private static void requireNonNegative(int value) {
        if (value < 0) {
            throw new IllegalArgumentException("value is negative: " + value);
        }
    }

    Money plus(Money other) {
        if (!this.currency.equals(other.currency)) {
            throw new IllegalArgumentException();
        }
        return new Money(this.value + other.value, this.currency);
    }
}
```

---

Refactor : la lisibilité prime !

```java
import java.util.Currency;
import java.util.Objects;

record Money(int value, Currency currency) {
    Money {
        requireNonNegative(value);
        Objects.requireNonNull(currency, "currency is null");
    }

    private static void requireNonNegative(int value) {
        if (value < 0) {
            throw new IllegalArgumentException("value is negative: " + value);
        }
    }

    Money plus(Money other) {
        Objects.requireNonNull(other, "other is null");
        requireSameCurrency(other);
        return new Money(this.value + other.value, this.currency);
    }

    private void requireSameCurrency(final Money other) {
        if (!this.currency.equals(other.currency)) {
            throw new IllegalArgumentException(String.format("%s and %s have different currencies", this, other));
        }
    }
}
```

---

### Avantages

* favorise la conception par contrat
* assure une bonne couverture de tests automatisés
* force à rendre le code testable

---

### Inconvénients

* mode de pensée à apprivoiser

{{% /section %}}
