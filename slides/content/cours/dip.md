+++

weight = 10

+++

{{% section %}}

## Dependency Inversion Principle (DIP)

De la testabilité en barre.

---

### Définition

---

1. Des modules de haut niveau ne devraient pas dépendre de modules de bas niveau. Les deux devraient dépendre
   d'abstractions.

--- 

2. Les abstractions ne devraient pas dépendre des détails. Les détails devraient dépendre des abstractions.

---

### Exemple 1

---

Violation du DIP

```java
import java.nio.ByteBuffer;

final class Keyboard {
    byte read() {
        // Reads keystrokes from the keyboard.
    }
}

final class Printer {
    void write(byte[] bytes) {
        // Sends the bytes to the printer.
    }
}

final class Copier {
    void copy() {
        var buffer = ByteBuffer.allocate(500);
        var keyboard = new Keyboard();
        var printer = new Printer();

        while (buffer.hasRemaining()) {
            buffer.put(keyboard.read());
        }

        printer.write(buffer.array());
    }
}
```

---

Inversion des dépendances

```java
import java.nio.ByteBuffer;

interface Reader {
    byte read();
}

interface Writer {
    void write(byte[] bytes);
}

final class Copier {
    void copy(Reader reader, Writer writer) {
        var buffer = ByteBuffer.allocate(500);

        while (buffer.hasRemaining()) {
            buffer.put(reader.read());
        }

        writer.write(buffer.array());
    }
}

final class Keyboard implements Reader {
    @Override
    public byte read() {
        // Reads keystrokes from the keyboard.
    }
}

final class Printer implements Writer {
    @Override
    public void write(byte[] bytes) {
        // Sends the bytes to the printer.
    }
}
```

---

### Exemple 2

---

Violation du DIP

```java
final class Lamp {
    void turnOn() {
        // Turns the lamp on.
    }

    void turnOff() {
        // Turns the lamp off.
    }
}

final class Button {
    private final Lamp lamp;

    Button(Lamp lamp) {
        this.lamp = lamp;
    }

    void detect() {
        if (isPressed()) {
            lamp.turnOn();
        } else {
            lamp.turnOff();
        }
    }

    private boolean isPressed() {
        // Tells whether it has been pressed.
    }
}
```

---

Inversion des dépendances

```java
interface Lamp {
    void turnOn();

    void turnOff();
}

final class FloodLight implements Lamp {
    @Override
    public void turnOn() {
        // Turns the floodlight on.
    }

    @Override
    public void turnOff() {
        // Turns the floodlight off.
    }
}

final class Button {
    private final Lamp lamp;

    Button(Lamp lamp) {
        this.lamp = lamp;
    }

    void detect() {
        if (isPressed()) {
            lamp.turnOn();
        } else {
            lamp.turnOff();
        }
    }

    private boolean isPressed() {
        // Tells whether it has been pressed.
    }
}
```

---

### L'injection de dépendances (DI)

La chasse au `new`.

```java
final class Copier {
    void copy() {
        var buffer = ByteBuffer.allocate(500);
        Reader reader = new Keyboard();
        Writer writer = new Printer();

        while (buffer.hasRemaining()) {
            buffer.put(reader.read());
        }

        writer.write(buffer.array());
    }
}
```

```java
final class Copier {
    private final Reader reader;
    private final Writer writer;

    Copier(Reader reader, Writer writer) {
        this.reader = reader;
        this.writer = writer;
    }

    void copy() {
        var buffer = ByteBuffer.allocate(500);

        while (buffer.hasRemaining()) {
            buffer.put(reader.read());
        }

        writer.write(buffer.array());
    }
}
```

---

À implémenter, en fonction de votre contexte :

* à la main ;
* avec un outil dédié (Spring Framework, Guice, picocontainer, _etc_).

{{% note %}}

Pour des petits projets ou un faible besoin de maintenance, il n'est pas toujours nécessaire de tirer tout un framework
pour faire l'injection de dépendances, d'autant que c'est une tâche assez simple.

La contrepartie de l'utilisation d'outils pour la DI est souvent un démarrage plus lent à cause d'une utilisation
massive de l'introspection.

{{% /note %}}

---

Toujours privilégier l'injection de dépendances **par le constructeur**.

Ainsi, le code reste testable même sans framework. Vos tests unitaires vous remercient.

{{% note %}}

À la fois la facilité d'écriture et la vitesse d'exécution, donc l'intérêt, des tests unitaires sont facilitées par
l'injection des dépendances par le constructeur.

{{% /note %}}

{{% /section %}}
