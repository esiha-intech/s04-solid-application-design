+++

weight = 1

+++

{{% section %}}

## Prérequis

Programmation Orientée Objet en Java

---

Classes, propriétés, méthodes, _etc._

```java
import java.util.Objects;

public final class Length implements Comparable<Length> {
    private final long valueMeters;

    private Length(long valueMeters) {
        this.valueMeters = valueMeters;
    }

    public static Length meters(long value) {
        return new Length(value);
    }

    @Override
    public int compareTo(Length o) {
        return Long.compare(this.valueMeters, o.valueMeters);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Length length = (Length) o;
        return valueMeters == length.valueMeters;
    }

    @Override
    public int hashCode() {
        return Objects.hash(valueMeters);
    }

    @Override
    public String toString() {
        return valueMeters + " m";
    }
}

```

{{% note %}}

* Classe
* Propriété
* Méthode de classe/d'instance
* Constructeur
* Interface
* Généricité
* Finalité
* Visibilité (`private`, default, `package` , `public`)

{{% /note %}}

---

Énumérations, Streams, Lambdas, _etc_.

```java
enum Heading {
    NORTH(0),
    EAST(90),
    SOUTH(180),
    WEST(270);

    private final int degrees;

    Heading(int degrees) {
        this.degrees = degrees;
    }

    Heading turn90DegreesRight() {
        return ofDegrees((degrees + 90) % 360);
    }

    Heading turn90DegreesLeft() {
        return ofDegrees((degrees + 270) % 360);
    }

    private static Heading ofDegrees(int degrees) {
        return Arrays.stream(values())
                .filter(h -> h.degrees == degrees)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("invalid degrees: " + degrees));
    }
}

```

{{% note %}}

* Énumérations
* Streams
* Lambdas
* Functional interfaces
* Predicates
* Optional
* Exceptions

{{% /note %}}

{{% /section %}}
