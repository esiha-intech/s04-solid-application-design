+++

weight = 11

+++

{{% section %}}

## État d'esprit

Le dogme et le code.

---

### La fenêtre cassée

Ou comment favoriser la déliquescence.

---

### Le cas des commentaires

Shame! Shame! Shame!

---

Usages couramment rencontrés :

| Peut-être utile                |                    Généralement inquiétant |
|--------------------------------|-------------------------------------------:|
| Documentation (javadoc, _etc_) |                         Traduction du code |
| Formuler une propriété du code |  Expliquer pourquoi le code est ainsi fait |

{{% note %}}

La documentation doit avoir un public cible réel, sinon elle est une charge de travail inutile.

Formuler une propriété du code de façon extra-linguistique, par exemple Java ne permet pas de dire d'une classe qu'elle
est une classe utilitaire. Le fait de rendre le constructeur privé avec un petit
commentaire `Utility class: not constructable` est utile. De même, contourner un bug d'une bibliothèque externe avec une
bidouille peut faire l'objet d'un commentaire tout aussi utile.

La traduction du code en anglais/français est pire qu'une perte de temps : elle encourage les personnes suivantes à lire
le commentaire plutôt que le code, même si ce dernier a évolué et que le commentaire n'est plus adapté.

Expliquer pourquoi le code est ainsi fait est généralement un aveu de faiblesse : « je n'ai pas été capable de rendre
mon code lisible ».

{{% /note %}}

---

Mon avis : un commentaire est un mensonge en puissance.

---

### Make it work, make it right, make it fast

Dans cet ordre.

---

* **Make it work** \
  Commencer par faire fonctionner sans trop se soucier de la maintenabilité
* **Make it right** \
  Ensuite, refactoriser pour améliorer la maintenabilité
* **Make it fast** \
  Enfin et seulement si c'est utile, améliorer les performances.

{{% note %}}

Le TDD offre une bonne façon de suivre ce précepte : les étapes Rouge et Verte sont le _make it work_. L'étape de
Refactorisation est le _make it right_.

Le _make it fast_ serait probablement à traiter au niveau global. Pour savoir si c'est utile d'améliorer les
performances, il faut commencer par les mesurer pour identifier là où le plus gros gain est à chercher.

{{% /note %}}

---

### Don't Repeat Yourself (DRY)

Moins de code ⇒ moins de bugs.

---

Pour qu'une déduplication soit justifiée, il faut :

* que le code fasse la même chose
* que le code soit pour la même chose.

{{% note %}}

Une bonne déduplication augmente la cohésion sans augmenter significativement le couplage.

Une déduplication qui oublie la 2e partie augmente fortement le couplage sans augmenter la cohésion.

{{% /note %}}

---

### Keep It Simple, Stupid (KISS)

Pensez au futur vous.

---

La solution ne devrait pas être plus compliquée que le problème.

Les Design Patterns sont avant tout des outils de refactorisation.

---

### You Ain't Gonna Need It (YAGNI)

Oui, mais peut-être que plus tard...

---

Le code n'est pas un lieu de divination.

Prenez des décisions le plus tard qu'il est intelligemment possible.

---

### "Foolish consistency is the hobgoblin of little minds"

Le mot clef, c'est « devrait ».

---

Il faut maîtriser les règles pour savoir quand les briser.

---

### Easy To Change (ETC)

Le point qui les englobe tous.

{{% /section %}}
