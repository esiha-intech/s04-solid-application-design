+++

weight = 9

+++

{{% section %}}

## Interface Segregation Principle (ISP)

Le régime, version interfaces.

---

### Définition

Un client ne devrait pas être forcé de dépendre de méthodes qu'il n'utilise pas.

{{% note %}}

Cette dépendance peut être matérialisée par l'existence de méthodes qu'il n'utilise pas dans une interface ou qu'il est
forcé d'implémenter pour correspondre au contrat d'une interface.

L'idée générale est de décourager la création d'interfaces fourre-tout.

{{% /note %}}

---

### Exemple 1

---

Prémisses

```java
interface Door {
    void lock();

    void unlock();

    boolean isOpen();
}

final class Timer {
    void register(int timeOutSeconds, TimerClient client) {
        // When timeOutSeconds pass, the client::timeOut method is called.
    }
}

interface TimerClient {
    void timeOut();
}
```

Violation de l'ISP

```java
interface Door extends TimerClient {
    // content elided
}

final class TimedDoor implements Door {
    // Implements lock, unlock, isOpen, and timeOut.
}

final class SimpleDoor implements Door {
    // Implements lock, unlock, isOpen. Also has to implement timeOut :(
}
```

---

Implémentation de multiples interfaces

```java
interface Door {
    void lock();

    void unlock();

    boolean isOpen();
}

final class Timer {
    void register(int timeOutSeconds, TimerClient client) {
        // When timeOutSeconds pass, the client::timeOut method is called.
    }
}

interface TimerClient {
    void timeOut();
}

final class TimedDoor implements Door, TimerClient {
    // Implements lock, unlock, isOpen, and timeOut.
}

final class SimpleDoor implements Door {
    // Only has to implement lock, unlock, and isOpen.
}
```

---

### Exemple 2

---

Violation de l'ISP

```java
interface BankAccount {
    void deposit(Money amount);

    Money getCurrentBalance();
}

final class FixedTermDepositAccount implements BankAccount {
    // Implements deposit and getCurrentBalance.
}

interface WithdrawableBankAccount extends BankAccount {
    void withdraw(Money amount);
}

final class CurrentAccount implements WithdrawableBankAccount {
    // Implements deposit, getCurrentBalance, and withdraw.
}

interface BankAccountService {
    void depositOnAccount(int accountId, Money amount);

    void withdrawFromAccount(int accountId, Money amount);

    Money getAccountCurrentBalance(int accountId);
}

final class CurrentAccountService implements BankAccountService {
    // Implements depositOnAccount, withdrawFromAccount, and getAccountCurrentBalance.
}

final class FixedTermDepositAccountService implements BankAccountService {
    // Implements depositOnAccount and getAccountCurrentBalance.
    // Also has to implement withdrawFromAccount :(
}
```

---

Séparation des interfaces

```java
interface BankAccountService {
    void depositOnAccount(int accountId, Money amount);

    Money getAccountCurrentBalance(int accountId);
}

final class FixedTermDepositAccountService implements BankAccountService {
    // Implements depositOnAccount and getAccountCurrentBalance.
}

interface WithdrawableBankAccountService extends BankAccountService {
    void withdrawFromAccount(int accountId, Money amount);
}

final class CurrentAccountService implements WithdrawableBankAccountService {
    // Implements depositOnAccount, withdrawFromAccount, and getAccountCurrentBalance.
}
```

{{% /section %}}
