+++

weight = 4

+++

{{% section %}}

## Forte cohésion, faible couplage

La modularisation utile, à toutes les échelles.

---

### Forte cohésion

Une classe ou un paquet devrait avoir des responsabilités cohérentes les unes avec les autres. Les éléments qui
constituent une classe ou un paquet devraient interagir fortement les uns avec les autres.

---

### Faible couplage

Une classe ou un paquet ne devrait pas dépendre des détails d'une autre classe ou paquet. Les interactions entre
différentes classes ou paquets devraient être minimisées.

---

### Exemple 1

---

Un grand classique, qui a l'air innocent...

![](layers_packages.png)

---

... mais dont les détails révèlent la catastrophe

![](layers_details.png)

---

Augmentation de la cohésion, diminution du couplage

![](features.png)

---

### Exemple 2

---

La fameuse classe `Utils`

```java
record Money(int value, Currency currency) {
    static Money sum(Money... monies) {
        var values = Arrays.stream(monies).map(Money::value).toList();
        return new Money(Utils.sum(values), monies[0].currency);
    }
}

final class Utils {
    static final String DEBUG = "DEBUG";
    static final String INFO = "INFO";
    static final String WARN = "WARN";
    static final String ERROR = "ERROR";

    static int sum(Collection<Integer> ints) {
        return ints.stream().mapToInt(Integer::intValue).sum();
    }

    static String formatLog(String level, String message) {
        return String.format("[%s] %s", level, message);
    }

    // More static methods manipulating many things
}
```

---

Extraction des concepts masqués

```java
record Money(int value, Currency currency) {
    static Money sum(Money... monies) {
        var value = Arrays.stream(monies).mapToInt(Money::value).sum();
        return new Money(value, monies[0].currency);
    }
}

final class Log {
    private Log() {
        // Utility class: not constructable
    }

    enum Level {
        DEBUG, INFO, WARN, ERROR
    }

    static String formatLog(Level level, String message) {
        return String.format("[%s] %s", level, message);
    }
}
```

{{% note %}}

La bonne déduplication est celle qui élimine les doublons de codes à *l'intention* identique, pas seulement la forme identique.

{{% /note %}}

{{% /section %}}
