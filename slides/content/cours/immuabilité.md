+++

weight = 3

+++

{{% section %}}

## Immuabilité

Ou immutabilité, c'est comme vous voulez.

---

Une innocente classe dans notre package `model`

```java
import java.util.Currency;
import java.util.Objects;

class Money {
    private int value;
    private Currency currency;

    Money(int value, Currency currency) {
        this.value = requireNonNegative(value);
        this.currency = Objects.requireNonNull(currency, "currency is null");
    }

    private static int requireNonNegative(int value) {
        if (value < 0) {
            throw new IllegalArgumentException("value is negative: " + value);
        }

        return value;
    }

    int getValue() {
        return value;
    }

    void setValue(int value) {
        this.value = value;
    }

    Currency getCurrency() {
        return currency;
    }

    void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
```

{{% note %}}

Petit brainstorm sur ce qui peut mal se passer avec un tel Java Bean.

Bogues à trouver:

* `setValue` ne vérifie pas la non-négativité de la valeur
* `setCurreny` ne vérifie pas la non-nullité de la valeur
* des sous-classes de Money peuvent redéfinir les setters
* des corruptions de données sont très faciles à faire si une même instance de Money est partagée entre plusieurs objets

{{% /note %}}

---

2 + 3 = 5

---

Une version immuable de cette même classe en Java 11

```java
import java.util.Currency;
import java.util.Objects;

final class Money {
    private final int value;
    private final Currency currency;

    Money(int value, Currency currency) {
        this.value = requireNonNegative(value);
        this.currency = Objects.requireNonNull(currency, "currency is null");
    }

    private static int requireNonNegative(int value) {
        if (value < 0) {
            throw new IllegalArgumentException("value is negative: " + value);
        }

        return value;
    }

    int getValue() {
        return value;
    }

    Currency getCurrency() {
        return currency;
    }
}
```

---

La même chose, mais avec les _records_ disponibles depuis Java 16

```java
import java.util.Currency;
import java.util.Objects;

record Money(int value, Currency currency) {
    Money {
        requireNonNegative(value);
        Objects.requireNonNull(currency, "currency is null");
    }

    private static void requireNonNegative(int value) {
        if (value < 0) {
            throw new IllegalArgumentException("value is negative: " + value);
        }
    }
}
```

---

### Conditions

* classe finale
* propriétés finales
* propriétés récursivement immuables

---

### Avantages

* raisonnement facilité
* _thread-safe_, donc partageable
* facilement testable
* favorise la création d'objets riches

---

### Inconvénients

* mode de pensée à apprivoiser
* en Java, des `final` à foison, sauf avec les _records_

---

### Attention

Tout ne gagne pas forcément à être immuable en Programmation Orientée Objet.

C'est une question de contexte et de cycle de vie.

{{% note %}}

Billet de banque, selon qu'on est un commerçant ou la Banque de France

{{% /note %}}

{{% /section %}}
