+++

weight = 2

+++

{{% section %}}

## Testabilité

Si c'est difficile, faites-le plus souvent.

---

### Pourquoi tester ?

{{% note %}}

Petit brainstorm sur l'utilité du test.

Points attendus, à compléter :

* valider les nouveaux développements
* vérifier si quelque chose d'existant a été cassé
* reproduire un bug
* définir les responsabilités/contrats
* mesurer les performances
* vérifier les cas aux limites
* vérifier la conformité à une réglementation/norme
* faciliter l'amélioration du code, le rendre facile à changer.

{{% /note %}}

---

### Pourquoi automatiser les tests ?

{{% note %}}

Petit brainstorm sur l'utilité de l'automatisation.

Points attendus, à compléter :

* gagner du temps
* livrer plus souvent
* économiser de l'argent
* limiter les erreurs humaines
* se concentrer sur des tâches plus intéressantes

{{% /note %}}

---

« Tester, ça sert à rien. »

« Tester, c'est douter. Douter, c'est échouer. »

{{% fragment %}}⇒ « Tester demande des efforts immédiats pour un gain futur et j'ai la flemme. »{{% /fragment %}}

---

« Ouais, mais ça, c'est pas testable ! »

{{% fragment %}}⇒ Le code nous dit « C'est pas fifou tout ça, tu peux pas faire mieux ? ».{{% /fragment %}}

{{% note %}}

Ça tombe bien, tout ce qu'on va voir dans la suite de ce cours vise à faciliter la testabilité, la maintenabilité,
l'évolutivité.

{{% /note %}}

{{% /section %}}
