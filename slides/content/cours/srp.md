+++

weight = 6

+++

{{% section %}}

## Single Responsibility Principle (SRP)

Il ne peut en rester qu'une.

---

### Définition

Une classe (ou méthode) ne devrait avoir qu'une seule responsabilité, ne faire qu'une seule chose.

---

### Exemple 1

---

Violation du SRP

```java
class BankAccountService {
    private final AccountRepository repository;

    BankAccountService(final AccountRepository repository) {
        this.repository = repository;
    }

    void withdraw(int accountId, Money amount) {
        var currentBalance = repository.getBalance(accountId);
        
        if (!currentBalance.currency().equals(amount.currency())) {
            throw new IllegalArgumentException(String.format("Currency mismatch: %s and %s",
                    this.currency, other.currency));
        }

        if (amount.value() > currentBalance.value()) {
            throw new IllegalArgumentException("Insufficient funds: " + currentBalance.value());
        }
        
        var newBalance = new Money(currentBalance.value() - amount.value(), currentBalance.currency());

        repository.setBalance(accountId, newBalance);
    }

}
```

{{% note %}}

Il y a 3 responsabilités principales dans `withdraw`:

* validation que le montant peut être soustrait
* soustraction du montant
* mécanique lecture/modification/sauvegarde

{{% /note %}}

---

Séparation des responsabilités

```java
record Money(int value, Currency currency) {
    // -- content elided

    Money minus(Money other) {
        requireCanSubtract(other);
        return new Money(this.value - other.value, this.currency);
    }

    private void requireCanSubtract(Money other) {
        if (!this.currency.equals(other.currency)) {
            throw new IllegalArgumentException(String.format("Currency mismatch: %s and %s",
                    this.currency, other.currency));
        }

        if (this.value < other.value) {
            throw new IllegalArgumentException("Resulting value would be negative");
        }
    }
}

class BankAccountService {
    // -- content elided

    void withdraw(int accountId, Money amount) {
        var currentBalance = repository.getBalance(accountId);
        repository.setBalance(accountId, currentBalance.minus(amount));
    }
}
```

{{% note %}}

Il est probable qu'il faudrait extraire la validation de devise dans une méthode dédiée : on va vouloir faire cette
vérification à pas mal d'endroits.

Ça pourrait aussi être une bonne idée, pour la lisibilité et la facilité de traitement, de créer des exceptions
dédiées : `IncompatibleCurrenciesException` et `NegativeMoneyValueException` par exemple.

{{% /note %}}

---

### Exemple 2

---

Violation du SRP

```java
record Money(
        @JsonProperty("val")
        int value,
       
        @JsonProperty("cur") @JsonUnwrapped @JsonRawValue
        Currency currency
) {
    // content elided
}
```

{{% note %}}

Les 2 responsabilités mélangées sont :

* la modélisation métier d'un montant ;
* les spécificités de sérialisation/dé-sérialisation en JSON.

{{% /note %}}

---

Séparation des responsabilités

```java
record Money(int value, Currency currency) {
    // content elided
}

record MoneyJsonDto(int val, String cur) {
    static MoneyJsonDto from(Money money) {
        return new MoneyJsonDto(money.value(), money.currency().getCurrencyCode());
    }
}
```

{{% note %}}

MoneyJsonDto est un Data Transfer Object (DTO).

{{% /note %}}

{{% /section %}}
