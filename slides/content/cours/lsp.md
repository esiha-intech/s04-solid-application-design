+++

weight = 8

+++

{{% section %}}

## Liskov Substitution Principle (LSP)

Une histoire de cohérence.

---

### Définition

Une propriété vraie pour les instances d'une classe A devrait le rester pour les instances d'une classe B, fille de A.

{{% note %}}

Autrement dit : un sous-type devrait pouvoir être utilisé en lieu et place de son type de base.

{{% /note %}}

---

### Exemple 1

---

Violation du LSP

```java
abstract sealed class Account permits CurrentAccount, FixedTermDepositAccount {
    abstract void deposit(Money amount);

    abstract void withdraw(Money amount);
}

final class CurrentAccount extends Account {
    @Override
    void deposit(final Money amount) {
        // deposit the provided amount
    }

    @Override
    void withdraw(final Money amount) {
        // withdraw the provided amount
    }
}

final class FixedTermDepositAccount extends Account {
    @Override
    void deposit(final Money amount) {
        // deposit the provided amount
    }

    @Override
    void withdraw(final Money amount) {
        throw new UnsupportedOperationException("Withdrawals are not supported by " + getClass().getSimpleName());
    }
}

class AccountService {
    void withdraw(int accountId, final Money amount) {
        Account account = findAccount(accountId);
        account.withdraw(amount);
    }
}
```

---

Mauvaises corrections

```java
class AccountService {
    void withdraw(int accountId, final Money amount) {
        Account account = findAccount(accountId);
        if (account instanceof FixedTermDepositAccount fixed) {
            // warn user the operation is denied
        } else {
            account.withdraw(money);
        }
    }
}
```

```java
class AccountService {
    void withdraw(int accountId, final Money amount) {
        Account account = findAccount(accountId);
        try {
            account.withdraw(money);
        } catch (UnsupportedOperationException e) {
            // warn user the operation is denied
        }
    }
}
```

---

Séparation des comportements

```java
abstract sealed class Account permits WithdrawableAccount, FixedTermDepositAccount {
    abstract void deposit(Money amount);
}

abstract sealed class WithdrawableAccount extends Account {
    abstract void withdraw(Money amount);
}

final class CurrentAccount extends WithdrawableAccount {
    @Override
    void deposit(final Money amount) {
        // deposit the provided amount
    }

    @Override
    void withdraw(final Money amount) {
        // withdraw the provided amount
    }
}

final class FixedTermDepositAccount extends Account {
    @Override
    void deposit(final Money amount) {
        // deposit the provided amount
    }
}

class AccountService {
    void withdraw(int accountId, final Money amount) {
        WithdrawableAccount account = findWithdrawableAccount(accountId);
        account.withdraw(money);
    }
}
```

---

### Exemple 2

---

Violation du LSP

```java
import java.util.ArrayList;
import java.util.List;

class Scratch {
    public static void main(String[] args) {
        System.out.println(addSize(new ArrayList<>()));
        System.out.println(addSize(List.of()));
    }

    static List<Integer> addSize(List<Integer> ints) {
        ints.add(ints.size());
        return ints;
    }
}
```

---

Séparation des comportements

```java
interface List<T> {
    T get(int index);

    int size();

    // other read-related operations elided
}

interface MutableList<T> extends List<T> {
    void add(T t);

    void clear();

    // other write-related operations elided
}
```

{{% /section %}}