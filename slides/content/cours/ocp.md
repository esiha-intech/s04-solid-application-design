+++

weight = 7

+++

{{% section %}}

## Open/Closed Principle (OCP)

L'abstraction, c'est la vie.

---

### Définition

Une entité (module, classe, fonction) devrait être ouverte à l'extension, fermée à la modification.

{{% note %}}

En d'autres mots, ajouter du comportement ne devrait pas mener à la modification d'une méthode existante.

{{% /note %}}

---

### Exemple 1

---

Violation de l'OCP

```java
record Point(int x, int y) {
}

record Circle(Point center, int radius) {
}

record Square(Point topLeftCorner, int side) {
}

final class Areas {
    private Areas() {
        // Utility class
    }

    static double sum(Object... shapes) {
        double area = 0;
        for (final Object shape : shapes) {
            if (shape instanceof Circle circle) {
                area += Math.PI * Math.pow(circle.radius(), 2);
            } else if (shape instanceof Square square) {
                area += Math.pow(square.side(), 2);
            } else {
                throw new IllegalArgumentException("Unknown shape type: " + shape.getClass());
            }
        }
        return area;
    }
}
```

---

Fermeture de `Areas.sum`

```java
import java.util.Arrays;

record Point(int x, int y) {
}

interface Shape {
    double computeArea();
}

record Circle(Point center, int radius) implements Shape {
    @Override
    public double computeArea() {
        return Math.PI * Math.pow(radius, 2);
    }
}

record Square(Point topLeftCorner, int side) implements Shape {
    @Override
    public double computeArea() {
        return Math.pow(side, 2);
    }
}

final class Areas {
    private Areas() {
        // Utility class
    }

    static double sum(Shape... shapes) {
        return Arrays.stream(shapes)
                .mapToDouble(Shape::computeArea)
                .sum();
    }
}
```

{{% note %}}

Plus d'exception à l'exécution → le compilateur fait le travail de vérification.

Plus de « shotgun surgery » consistant à modifier le logiciel un peu partout pour ajouter un comportement.

{{% /note %}}

---

### Exemple 2

---

Violation de l'OCP

```java
import java.util.*;

interface Thing {
}

record Fridge() implements Thing {
}

record CardboardBox() implements Thing {
}

record Pillow() implements Thing {
}

class Truck {
    private final List<Thing> cargo = new ArrayList<>();

    void load(Collection<Thing> things) {
        for (final var type : List.of(Fridge.class, CardboardBox.class, Pillow.class)) {
            load(things, type);
        }
    }

    private void load(final Collection<Thing> things, final Class<? extends Thing> type) {
        things.stream().filter(type::isInstance).forEach(cargo::add);
    }
}
```

---

Fermeture de `Truck.load`

```java
import java.util.*;

interface Thing {
}

record Fridge() implements Thing {
}

record CardboardBox() implements Thing {
}

record Pillow() implements Thing {
}

final class LoadingOrder implements Comparator<Thing> {
    private static final List<Class<? extends Thing>> LOADING_ORDER = List.of(
            Fridge.class,
            CardboardBox.class,
            Pillow.class
    );

    @Override
    public int compare(final Thing one, final Thing other) {
        return Integer.compare(loadingOrder(one), loadingOrder(other));
    }

    private static int loadingOrder(Thing thing) {
        final var index = LOADING_ORDER.indexOf(thing.getClass());
        if (index < 0) {
            throw new IllegalStateException("Missing loading order for: " + thing.getClass());
        }
        return index;
    }
}

class Truck {
    private final List<Thing> cargo = new ArrayList<>();

    void load(Collection<Thing> things) {
        things.stream().sorted(new LoadingOrder()).forEach(cargo::add);
    }
}
```

{{% note %}}

La seule trace d'ouverture restante est explicite, unique, réutilisable.

{{% /note %}}

{{% /section %}}